# Credits

Uses keccak sha implementation from [libethereum](https://github.com/ethereum/libethereum) (sha3.c, sha3.h and compiler.h)

# Build

Prerequisites

```
sudo apt-get install php5-dev
```

Compile

```
phpize
./configure --enable-keccak CFLAGS='-g -O2 -std=c99'
make
```

Install

Copy `modules/keccak.so` to `extension_dir`

```
sudo cp modules/keccak.so `php -r "echo ini_get('extension_dir').\"\n\";"`
```

Enable

Append `extension=keccak.so` to php.ini

# Usage

```php
echo keccak_sha256('');
// prints c5d2460186f7233c927e7db2dcc703c0e500b653ca82273b7bfad8045d85a470
```
