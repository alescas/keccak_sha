PHP_ARG_ENABLE(keccak,
[Whether to enable Keccak support],
[--enable-keccak           Enable Keccak support])

if test "$PHP_KECCAK" != "no"; then
    PHP_NEW_EXTENSION(keccak, src/php_keccak.c src/sha3.c, $ext_shared)
fi
