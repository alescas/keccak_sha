#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "php.h"
#include "sha3.h"
#include "ext/standard/info.h"
#include "ext/hash/php_hash.h"
#include "php_keccak.h"

#define PHP_KECCAK_NAME "keccak"
#define PHP_KECCAK_VERSION "0.1.0"
#define PHP_KECCAK_STANDARD_VERSION "FIPS 202"

zend_function_entry keccak_functions[] = {
    PHP_FE(keccak_sha256, NULL)
    PHP_FE_END
};

PHP_MINFO_FUNCTION(keccak_sha256)
{
    php_info_print_table_start();
    php_info_print_table_row(2, "keccak support", "enabled");
    php_info_print_table_row(2, "extension version",  PHP_KECCAK_VERSION);
    php_info_print_table_row(2, "standard version", PHP_KECCAK_STANDARD_VERSION);
    php_info_print_table_end();
}

zend_module_entry keccak_module_entry = {
#if ZEND_MODULE_API_NO >= 20010901
    STANDARD_MODULE_HEADER,
#endif
    PHP_KECCAK_NAME,
    keccak_functions,
    NULL,
    NULL,
    NULL,
    NULL,
    PHP_MINFO(keccak_sha256),
#if ZEND_MODULE_API_NO >= 20010901
     PHP_KECCAK_VERSION,
#endif
    STANDARD_MODULE_PROPERTIES
};

#ifdef COMPILE_DL_KECCAK
ZEND_GET_MODULE(keccak)
#endif

PHP_FUNCTION(keccak_sha256)
{
  char *data;
  int dataByteLength;
  const int hashByteLength = 32;

  if (zend_parse_parameters(ZEND_NUM_ARGS() TSRMLS_CC, "s", &data, &dataByteLength) == FAILURE) {
      return;
  }

  uint8_t hashVal[hashByteLength];
  int ret = sha3_256(hashVal, sizeof(hashVal), (uint8_t*)data, dataByteLength);

  if (ret!=0) {
    RETURN_FALSE;
  }

  char *hexDigest = safe_emalloc(hashByteLength, 2, 1);

  php_hash_bin2hex(hexDigest, hashVal, hashByteLength);
  hexDigest[2 * 32] = 0;

  RETURN_STRINGL(hexDigest, hashByteLength * 2, 1);
}






