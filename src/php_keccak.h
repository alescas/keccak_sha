#ifndef PHP_KECCAK_H
#define PHP_KECCAK_H 1
#ifdef ZTS
#include "TSRM.h"
#endif

ZEND_BEGIN_MODULE_GLOBALS(keccak)
    long counter;
    zend_bool direction;
ZEND_END_MODULE_GLOBALS(keccak)

#ifdef ZTS
#define KECCAK_G(v) TSRMG(keccak_globals_id, zend_keccak_globals *, v)
#else
#define KECCAK_G(v) (keccak_globals.v)
#endif

#define PHP_KECCAK_WORLD_VERSION "1.0"
#define PHP_KECCAK_WORLD_EXTNAME "keccak"

PHP_MINIT_FUNCTION(keccak);
PHP_MSHUTDOWN_FUNCTION(keccak);
PHP_RINIT_FUNCTION(keccak);

PHP_FUNCTION(keccak_sha256);

extern zend_module_entry keccak_module_entry;
#define phpext_keccak_ptr &keccak_module_entry

#endif
